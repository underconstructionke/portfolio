<!DOCTYPE HTML>
<html lang="en-US">

<!-- Mirrored from getmasum.com/html-preview/emeli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 18 Apr 2019 06:22:03 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mutisya - Personal Resume & Portfolio Template</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700,800" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/meanmenu.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/themify-icons.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/venobox.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}"/>
    <!-- CSS FOR COLOR SWITCHER -->
    <link rel="stylesheet" href="{{ asset('css/switcher/switcher.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/switcher/style1.css') }}" id="colors"/>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<!-- START PRELOADER -->
<div class="preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<!-- END PRELOADER -->

<!-- Star Header Area -->
<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-4">
                <!-- Logo Area -->
                <div class="logo_area">
                    <a href="#"><img src="img/logo.png" alt="Logo"/></a>
                </div>
            </div>

            <!-- Main Menu Area -->
            <div class="col-md-8 col-xs-8 menu_wrap">
                <nav id="navigation" class="navbar-right">
                    <ul id="nav" class="nav navbar-nav">
                        <li><a href="/">Home</a></li>
                        <li><a href="#service_area">Services</a></li>
                        <li><a href="#work_area">Works</a></li>
                        <li><a href="#resume_area">Resume</a></li>
                        <li><a href="#testimonial_area">testimonials</a></li>
                        <li><a href="#blog_area">Blog</a></li>
                        <li><a href="#contact_area">Contact</a></li>
                    </ul>
                </nav>

                <nav id="mobile_menu">
                    <ul class="nav navbar-nav">
                        <li><a href="/">Home</a></li>
                        <li><a href="#service_area">Services</a></li>
                        <li><a href="#work_area">Works</a></li>
                        <li><a href="#resume_area">Resume</a></li>
                        <li><a href="#testimonial_area">testimonials</a></li>
                        <li><a href="#blog_area">Blog</a></li>
                        <li><a href="#contact_area">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</header>
<!-- End header area -->


<!-- Start Banner Area -->
<section id="home_banner_area" class="text-left">
    <div class="container">
        <div class="single-slide-item-table">
            <div class="single-slide-item-tablecell">
                <div class="row">

                    <div class="col-md-5 hidden-sm hidden-xs">
                        <div class="banner-image">
                            <img src="mutisya-removebg.png" alt="">
                        </div>
                    </div><!-- End Col -->

                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="slider_content">
                            <h1>Hello I'm Ken.<br> <span class="typed"
                                                         data-elements="A Creative Web Designer,A Laravel Developer,A cloud Enthusiast"
                                ></span></h1>

                            <a href="#" class="main-btn ">Download CV</a>

                        </div>
                    </div><!-- End Col -->
                    {{--
                    --}}
                </div>

            </div>
        </div>
    </div>
</section>
<!-- End Banner Area -->
<div class="clearfix"></div>

<!-- Start About Area -->
<section id="about_area" class="section_padding">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-7 col-xs-12">
                <div class="about_content">
                    <h2 class="about_title">About Me</h2>
                    <p>
                        "I love what I do, and when you love what you do, you want to be the best at it."

                        I am a full stack developer with a variety of software development skills including LAMP and
                        MEAN Stack Development, API integrations, architecting custom solutions, and creatively solving
                        problems.

                        For the last decade, I have specialized in Enterprise-Level web and mobile applications with a
                        handful of clients in a variety of industries, ranging from startups to enterprises. I got my
                        start working on simple LAMP-Stack Financial Management System. Since then, I have progressed to
                        building and working with large, highly-customized and integrated platforms, becoming an expert
                        in full-stack development. I have also had the opportunity to work extensively with many other
                        frameworks and API's. I excel at meeting goals, making deadlines and consistently challenging
                        myself to learn new things. I am both detail and goal oriented and thrive in a fast-paced
                        environment

                    </p>
                    <img class="about-signature" src="img/sign.png" alt="signature"/>
                    <div class="about-buttons">
                        <a href="#" class="main-btn">Hire Me</a>
                        <a href="http://www.youtu.be/94yLIKZ-HjU" data-autoplay="true" data-vbtype="video"
                           class="video-popup vide_btn vbox-item"><i class="fa fa-play"></i> Watch Video</a>
                    </div>
                </div>
            </div>

            <div class="col-md-5 col-sm-5 col-xs-12">
                <div class="about_img">
                    <img src="img/about.png" class="img-responsive" alt="About Image">
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End About Area -->


<!-- Start Skill Area -->
<section id="skill_area" class="section_padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 text-center">
                <div class="section_title ">
                    <h2>My Skills</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable <br>content of a
                        page when looking at its layout</p>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="progress-bar-linear pr-15">
                    <p class="progress-bar-text">
                        Vue JS
                        <span>95%</span>
                    </p>
                    <div class="progress-bar">
                        <span data-percent="95"></span>
                    </div>
                </div><!--end progress bar-->

                <div class="progress-bar-linear pr-15">
                    <p class="progress-bar-text">
                        HTML5
                        <span>90%</span>
                    </p>
                    <div class="progress-bar">
                        <span data-percent="90"></span>
                    </div>
                </div><!--end progress bar-->
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="progress-bar-linear pr-15">
                    <p class="progress-bar-text">
                        CSS3
                        <span>95%</span>
                    </p>
                    <div class="progress-bar">
                        <span data-percent="95"></span>
                    </div>
                </div><!--end progress bar-->

                <div class="progress-bar-linear pr-15">
                    <p class="progress-bar-text">
                        Bootstrap
                        <span>90%</span>
                    </p>
                    <div class="progress-bar">
                        <span data-percent="90"></span>
                    </div>
                </div><!--end progress bar-->
            </div>
            <!--- END Col -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="progress-bar-linear pl-15">
                    <p class="progress-bar-text">
                       PHP
                        <span>90%</span>
                    </p>
                    <div class="progress-bar">
                        <span data-percent="90"></span>
                    </div>
                </div><!--end progress bar-->

                <div class="progress-bar-linear pl-15">
                    <p class="progress-bar-text">
                        Laravel
                        <span>95%</span>
                    </p>
                    <div class="progress-bar">
                        <span data-percent="95"></span>
                    </div>
                </div>
            </div><!--- END Col -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="progress-bar-linear pr-15">
                    <p class="progress-bar-text">
                        Database Development, Utilization, Optimization and Administration
                        <span>95%</span>
                    </p>
                    <div class="progress-bar">
                        <span data-percent="95"></span>
                    </div>
                </div><!--end progress bar-->

                <div class="progress-bar-linear pr-15">
                    <p class="progress-bar-text">
                        Bootstrap
                        <span>90%</span>
                    </p>
                    <div class="progress-bar">
                        <span data-percent="90"></span>
                    </div>
                </div><!--end progress bar-->
            </div>

        </div>
    </div>
</section>
<!-- End Skill Area -->

<!-- Start Service Area -->
<section id="service_area" class="section_padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 text-center">
                <div class="section_title ">
                    <h2>Our Services</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable <br>content of a
                        page when looking at its layout</p>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="service_details text-left wow fadeIn">
                <div class="col-md-4 col-sm-6">
                    <div class="serviceBox">
                        <i class="ti-light-bulb"></i>
                        <div class="ser_content">
                            <h3 class="title">App Development</h3>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro reprehenderit
                            </p>
                        </div>
                        <span class="ser-number">01</span>
                    </div>
                </div><!-- End Col -->

                <div class="col-md-4 col-sm-6">
                    <div class="serviceBox">
                        <i class="ti-stats-up"></i>
                        <div class="ser_content">
                            <h3 class="title">Web Development</h3>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro reprehenderit
                            </p>
                        </div>
                        <span class="ser-number">02</span>
                    </div>
                </div><!-- End Col -->

                <div class="col-md-4 col-sm-6">
                    <div class="serviceBox">
                        <i class="ti-vector"></i>
                        <div class="ser_content">
                            <h3 class="title">Responsive design</h3>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro reprehenderit
                            </p>
                        </div>
                        <span class="ser-number">03</span>
                    </div>
                </div><!-- End Col -->

                <div class="col-md-4 col-sm-6">
                    <div class="serviceBox">
                        <i class="ti-announcement"></i>
                        <div class="ser_content">
                            <h3 class="title">Designs & interfaces</h3>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro reprehenderit
                            </p>
                        </div>
                        <span class="ser-number">04</span>
                    </div>
                </div><!-- End Col -->

                <div class="col-md-4 col-sm-6">
                    <div class="serviceBox">
                        <i class="ti-blackboard"></i>
                        <div class="ser_content">
                            <h3 class="title">Highly customizable</h3>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro reprehenderit
                            </p>
                        </div>
                        <span class="ser-number">05</span>
                    </div>
                </div><!-- End Col -->

                <div class="col-md-4 col-sm-6">
                    <div class="serviceBox">
                        <i class="ti-headphone"></i>
                        <div class="ser_content">
                            <h3 class="title">Dedicated support</h3>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro reprehenderit
                            </p>
                        </div>
                        <span class="ser-number">06</span>
                    </div>
                </div><!-- End Col -->
            </div>
        </div>
    </div>
</section>
<!-- End Service Area -->

<!-- Start Work Area -->
<section id="work_area" class="our_works_area">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 text-center">
                <div class="section_title">
                    <h2>Latest Work</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable <br>content of a
                        page when looking at its layout</p>
                </div>
            </div>
        </div>

        <div class="row text-center">
            <div class="portfolio_filter">
                <ul>
                    <li class="active filter" data-filter="all">All</li>
                    <li class="filter" data-filter=".web_design">WEB DESIGN</li>
                    <li class="filter" data-filter=".illustrator">ILLUSTRATOR</li>
                    <li class="filter" data-filter=".ux_design">UL/UX DESIGN</li>
                    <li class="filter" data-filter=".print">PRINT</li>
                    <li class="filter" data-filter=".video">VIDEO</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="portfolio_item text-center">
                <div class="col-md-4 col-sm-6 mix web_design video ux_design">
                    <div class="single_portfolio">
                        <div class="port-box-content">
                            <img src="img/portfolio/1.jpg" alt="">
                            <a href="img/portfolio/1.jpg" class="port-icon lightbox" data-gall="gall-work"><i
                                        class="ti-plus"></i></a>
                        </div>
                        <div class="port-text text-left">
                            <h3 class="title"><a href="#">Portfolio Title</a></h3>
                            <span class="post">Web Design</span>
                        </div>
                    </div>
                </div> <!-- End Col -->

                <div class="col-md-4 col-sm-6 mix web_design video">
                    <div class="single_portfolio">
                        <div class="port-box-content">
                            <img src="img/portfolio/2.jpg" alt="">
                            <a href="img/portfolio/2.jpg" class="port-icon lightbox" data-gall="gall-work"><i
                                        class="ti-plus"></i></a>
                        </div>
                        <div class="port-text text-left">
                            <h3 class="title"><a href="#">Portfolio Title</a></h3>
                            <span class="post">Web Design</span>
                        </div>
                    </div>
                </div> <!-- End Col -->

                <div class="col-md-4 col-sm-6 mix web_design print ux_design">
                    <div class="single_portfolio">
                        <div class="port-box-content">
                            <img src="img/portfolio/3.jpg" alt="">
                            <a href="img/portfolio/3.jpg" class="port-icon lightbox" data-gall="gall-work"><i
                                        class="ti-plus"></i></a>
                        </div>
                        <div class="port-text text-left">
                            <h3 class="title"><a href="#">Portfolio Title</a></h3>
                            <span class="post">Web Design</span>
                        </div>
                    </div>
                </div> <!-- End Col -->

                <div class="col-md-4 col-sm-6 mix web_design illustrator ux_design">
                    <div class="single_portfolio">
                        <div class="port-box-content">
                            <img src="img/portfolio/4.jpg" alt="">
                            <a href="img/portfolio/4.jpg" class="port-icon lightbox" data-gall="gall-work"><i
                                        class="ti-plus"></i></a>
                        </div>
                        <div class="port-text text-left">
                            <h3 class="title"><a href="#">Portfolio Title</a></h3>
                            <span class="post">Web Design</span>
                        </div>
                    </div>
                </div> <!-- End Col -->

                <div class="col-md-4 col-sm-6 mix web_design illustrator">
                    <div class="single_portfolio">
                        <div class="port-box-content">
                            <img src="img/portfolio/5.jpg" alt="">
                            <a href="img/portfolio/5.jpg" class="port-icon lightbox" data-gall="gall-work"><i
                                        class="ti-plus"></i></a>
                        </div>
                        <div class="port-text text-left">
                            <h3 class="title"><a href="#">Portfolio Title</a></h3>
                            <span class="post">Web Design</span>
                        </div>
                    </div>
                </div> <!-- End Col -->

                <div class="col-md-4 col-sm-6 mix print">
                    <div class="single_portfolio">
                        <div class="port-box-content">
                            <img src="img/portfolio/6.jpg" alt="">
                            <a href="img/portfolio/6.jpg" class="port-icon lightbox" data-gall="gall-work"><i
                                        class="ti-plus"></i></a>
                        </div>
                        <div class="port-text text-left">
                            <h3 class="title"><a href="#">Portfolio Title</a></h3>
                            <span class="post">Web Design</span>
                        </div>
                    </div>
                </div> <!-- End Col -->
            </div>

            <div class="col-md-12 text-center">
                <a href="#" class="main-btn">Load More</a>
            </div>
        </div>

    </div>
</section>
<!-- End Work Area -->


<!-- Start Resume Area -->
<section id="resume_area" class="our_resume_area section_padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 text-center">
                <div class="section_title">
                    <h2>Our Resume</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable <br>content of a
                        page when looking at its layout</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 wow fadeIn">
                <h4 class="sing_resume_title"><i class="ti-crown"></i> Education</h4>
                <div class="single_resume">
                    <span class="resume_duration">March 2013 - 2017</span>
                    <h3>Computer Science</h3>
                    <span class="resume-designation">International University</span>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidiei dunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud .
                    </p>

                </div><!-- End Resume -->

                <div class="single_resume">
                    <span class="resume_duration">March 2012 - 2018</span>
                    <h3> Graphic Design </h3>
                    <span class="resume-designation">University of California</span>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidiei dunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud .
                    </p>

                </div><!-- End Resume -->

            </div><!-- End Col -->

            <div class="col-sm-6 wow fadeIn">
                <h4 class="sing_resume_title"><i class="ti-briefcase"></i> Experience</h4>
                <div class="single_resume">
                    <span class="resume_duration">March 2010 - 2019</span>
                    <h3>Behance</h3>
                    <span class="resume-designation">Senior UI UX Designer</span>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidiei dunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud .
                    </p>

                </div><!-- End Resume -->

                <div class="single_resume">
                    <span class="resume_duration">March 2012 - 2019</span>
                    <h3>Themeforest </h3>
                    <span class="resume-designation">WordPress Theme Developer</span>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidiei dunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud .
                    </p>
                </div><!-- End Resume -->
            </div><!-- End Col -->
        </div>
    </div>
</section>
<!-- End Resume Area -->


<!-- Start Counter Area -->
<div id="counter_area">
    <div class="container-fluid">
        <div class="text-center">
            <div class="col-md-3 col-sm-6">
                <div class="counter-items">
                    <div class="number-number">
                        <span class="counter">583</span>
                    </div>
                    <p class="number-desc">Projects Delivered</p>
                </div>
            </div><!--End Col -->

            <div class="col-md-3 col-sm-6">
                <div class="counter-items">
                    <div class="number-number">
                        <span class="counter">6924</span>
                    </div>
                    <p class="number-desc">Happy Customer</p>
                </div>
            </div><!--End Col -->

            <div class="col-md-3 col-sm-6">
                <div class="counter-items">
                    <div class="number-number">
                        <span class="counter">524</span>
                    </div>
                    <p class="number-desc">Downloads</p>
                </div>
            </div><!--End Col -->

            <div class="col-md-3 col-sm-6">
                <div class="counter-items">
                    <div class="number-number">
                        <span class="counter">9372</span>
                    </div>
                    <p class="number-desc">Awards Win</p>
                </div>
            </div><!--End Col -->
        </div>
    </div>
</div>
<!-- End Counter Area -->

<!-- Start Testmonial Area -->
<section id="testimonial_area" class="section_padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 text-center">
                <div class="section_title">
                    <h2>Our Testimonial</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable <br>content of a
                        page when looking at its layout</p>
                </div>
            </div>
        </div>

        <div class="row text-center">
            <div class="col-md-8 center-block">
                <div class="test_slide_area owl-carousel">
                    <div class="testimonial">
                        <div class="pic">
                            <img src="img/testimonial/1.jpg" alt="">
                        </div>

                        <div class="description">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor incidiei dunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam quis nostrud .
                            </p>
                        </div>

                        <div class="test_title_des">
                            <h3 class="testimonial-title">Christopher Ryan</h3>
                            <span class="test_designation">Designer</span>
                            <div class="test-review">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                    </div><!-- end Single Testimonials -->

                    <div class="testimonial">
                        <div class="pic">
                            <img src="img/testimonial/3.jpg" alt="">
                        </div>

                        <div class="description">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor incidiei dunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam quis nostrud .
                            </p>
                        </div>

                        <div class="test_title_des">
                            <h3 class="testimonial-title">Christopher Ryan</h3>
                            <span class="test_designation">Designer</span>
                            <div class="test-review">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                    </div><!-- end Single Testimonials -->

                    <div class="testimonial">
                        <div class="pic">
                            <img src="img/testimonial/3.jpg" alt="">
                        </div>

                        <div class="description">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor incidiei dunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam quis nostrud .
                            </p>
                        </div>

                        <div class="test_title_des">
                            <h3 class="testimonial-title">Christopher Ryan</h3>
                            <span class="test_designation">Designer</span>
                            <div class="test-review">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                    </div><!-- end Single Testimonials -->


                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Testmonial Area -->

<!-- Start Blog Area -->
<section id="blog_area" class="section_padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 text-center">
                <div class="section_title">
                    <h2>Latest Blog</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable <br>content of a
                        page when looking at its layout</p>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">
                <div class="single_post">
                    <div class="post-img">
                        <img src="img/blog/1.jpg" alt="">
                    </div>

                    <div class="blog_content">
                        <ul class="post-bar">
                            <li class="post-date"><i class="fa fa-calendar"></i> January 7, 2019</li>
                            <li class="author"><i class="fa fa-user"></i> <a href="#">admin</a></li>
                        </ul>
                        <h5 class="post-title"><a href="#">Group of people watching on laptop</a></h5>
                        <p class="post-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultrices felis in orci
                            condimentum, at viverra
                        </p>
                    </div>
                </div>
            </div>    <!-- End Col -->

            <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">
                <div class="single_post">
                    <div class="post-img">
                        <img src="img/blog/2.jpg" alt="">
                    </div>

                    <div class="blog_content">
                        <ul class="post-bar">
                            <li class="post-date"><i class="fa fa-calendar"></i> February 15, 2019</li>
                            <li class="author"><i class="fa fa-user"></i> <a href="#">admin</a></li>
                        </ul>
                        <h5 class="post-title"><a href="#">A Handsam man holding a book for Study</a></h5>
                        <p class="post-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultrices felis in orci
                            condimentum, at viverra
                        </p>
                    </div>
                </div>
            </div>    <!-- End Col -->

            <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">
                <div class="single_post">
                    <div class="post-img">
                        <img src="img/blog/3.jpg" alt="">
                    </div>

                    <div class="blog_content">
                        <ul class="post-bar">
                            <li class="post-date"><i class="fa fa-calendar"></i> March 28, 2019</li>
                            <li class="author"><i class="fa fa-user"></i> <a href="#">admin</a></li>
                        </ul>
                        <h5 class="post-title"><a href="#">photo of group of people in a meeting</a></h5>
                        <p class="post-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultrices felis in orci
                            condimentum, at viverra
                        </p>
                    </div>
                </div>
            </div><!-- End Col -->

        </div>
    </div>
</section>
<!-- End Blog Area -->

<!-- Start Client Area -->
<div id="clients" class="client_area">
    <div class="container">
        <div class="col-md-12">
            <div class="client_slide_area owl-carousel">
                <div class="single_client">
                    <a href="#"><img class="img-responsive" src="img/client/1.png" alt=""/></a>
                </div>
                <div class="single_client">
                    <a href="#"><img class="img-responsive" src="img/client/2.png" alt=""/></a>
                </div>
                <div class="single_client">
                    <a href="#"><img class="img-responsive" src="img/client/3.png" alt=""/></a>
                </div>
                <div class="single_client">
                    <a href="#"><img class="img-responsive" src="img/client/4.png" alt=""/></a>
                </div>
                <div class="single_client">
                    <a href="#"><img class="img-responsive" src="img/client/5.png" alt=""/></a>
                </div>

                <div class="single_client">
                    <a href="#"><img class="img-responsive" src="img/client/1.png" alt=""/></a>
                </div>
                <div class="single_client">
                    <a href="#"><img class="img-responsive" src="img/client/2.png" alt=""/></a>
                </div>
                <div class="single_client">
                    <a href="#"><img class="img-responsive" src="img/client/3.png" alt=""/></a>
                </div>
                <div class="single_client">
                    <a href="#"><img class="img-responsive" src="img/client/4.png" alt=""/></a>
                </div>
                <div class="single_client">
                    <a href="#"><img class="img-responsive" src="img/client/5.png" alt=""/></a>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- End Client Area -->

<!-- Start Contact Area -->
<section id="contact_area" class="section_padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 text-center">
                <div class="section_title">
                    <h2>Contact Me</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable <br>content of a
                        page when looking at its layout</p>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-7">
                <div class="contact_form">
                    <form id="contact-form" method="post" action="http://getmasum.com/html-preview/emeli/contact.php"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <input type="text" name="name" class="form-control" id="first-name"
                                       placeholder="Full Name" required="required">
                            </div>

                            <div class="form-group col-md-4">
                                <input type="email" name="email" class="form-control" id="emailn" placeholder="Email"
                                       required="required">
                            </div>

                            <div class="form-group col-md-4">
                                <input type="text" name="subject" class="form-control" id="subject"
                                       placeholder="Subject" required="required">
                            </div>

                            <div class="form-group col-md-12">
                                <textarea rows="6" name="message" class="form-control" id="description"
                                          placeholder="Your Message" required="required"></textarea>
                            </div>

                            <div class="col-md-12 text-center">
                                <div class="actions">
                                    <input type="submit" value="Send Me" name="submit" id="submitButton"
                                           class="main-btn"/>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div><!-- End Coll -->

            <div class="col-sm-5">
                <div class="contact_info">
                    <div class="single_con">
                        <i class="ti-map-alt"></i>
                        <p>3010 Raoul Wallenberg <br> PlaceWallingford CT 06492</p>
                    </div>

                    <div class="single_con">
                        <i class="ti-headphone-alt"></i>
                        <p>
                            203-906-9388<br/>
                            205-806-9368
                        </p>
                    </div>

                    <div class="single_con">
                        <i class="ti-email"></i>
                        <p>
                            yourmail@example.com<br/>
                            your@mail.com
                        </p>
                    </div>

                </div>
            </div><!-- End Coll -->

        </div>
    </div>
</section>
<!-- End Contact Area -->


<!-- Start Footer Area -->
<footer id="footer_area">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <p>Copyright © 2019 <a href="#">Emeli</a>, All rights Reserved.<br>
                    Created by Themes_Vila</p>
                <div class="footer_social_icons">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer Area -->

<!-- STYLE SWITCHER -->
<div id="style-switcher">
    <h2>Color Palette<a href="#"><i class="fa fa-cog fa-spin"></i></a></h2>
    <div>
        <ul class="colors" id="color1">
            <li><a href="#" class="style1"></a></li>
            <li><a href="#" class="style2"></a></li>
            <li><a href="#" class="style3"></a></li>
            <li><a href="#" class="style4"></a></li>
            <li><a href="#" class="style5"></a></li>
            <li><a href="#" class="style6"></a></li>
        </ul>
    </div>
</div>
<!-- END OF STYLE SWITCHER -->

<script src="{{ asset('js/jquery-1.12.4.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.nav.js') }}"></script>
<script src="{{ asset('js/jquery.meanmenu.js') }}"></script>
<script src="{{ asset('js/jquery.easing.1.3.min.js') }}"></script>
<script src="{{ asset('js/jquery.mixitup.js') }}"></script>
<script src="{{ asset('js/waypoints.min.js') }}"></script>
<script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/venobox.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/jquery.appear.js') }}"></script>
<script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
<script src="{{ asset('js/scrolltopcontrol.js') }}"></script>
<script src="{{ asset('js/typed.js') }}"></script>
<script src="{{ asset('js/switcher.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
</body>

<!-- Mirrored from getmasum.com/html-preview/emeli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 18 Apr 2019 06:23:36 GMT -->
</html>